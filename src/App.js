import './App.css';
import React from "react";
import Table from 'react-bootstrap/Table'


const axios = require('axios');

const BASE_URL = "https://api.horisol.pk/";



function App() {
  const [post, setPost] = React.useState(null);
  const users = [];

  React.useEffect(() => {
    axios.get(BASE_URL+'users').then((response) => {
      setPost(response.data);

    });
  }, []);
  if (!post) return null;
  Object.entries(post).map(([key, value]) => (
    users.push(post[key])
  ))
  
  return (
    <div className="App">
      <h1>DevOps Team Page Spinnaker</h1>
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={index + 1}>
              <th scope="row">{index + 1}</th>
              <td>{user.first_name}</td>
              <td>{user.last_name}</td>
              <td>{user.email}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );


}



export default App;
